'use strict';
require('dotenv').config({ debug: true })
const Hapi = require('@hapi/hapi');
const accountsPlugin = require('./src/plugins/accounts')
const announcementsPlugin = require('./src/plugins/announcements')
const init = async () => {

    const server = Hapi.server({
        port: 5000,
        host: 'localhost'||"0.0.0.0"
    });

    await server.register([ accountsPlugin,announcementsPlugin ])

    await server.start()
};

process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});

init();