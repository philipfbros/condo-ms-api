const Joi = require("joi");

module.exports = {
  headers: Joi.object({
    "x-api-key": Joi.string().required(),
  }),
  params: Joi.object({
    accountId: Joi.string().required(),
  }),
  options: {
    allowUnknown: true,
  },
};
