const Joi = require("joi");

module.exports = {
    headers: Joi.object({
      'x-api-key': Joi.string().required(),
      'user-token': Joi.string().required(),
    }),
  payload: Joi.object({
    first_name: Joi.string().required(),
    last_name: Joi.string().required(),
    username: Joi.string().required(),
    password: Joi.string().required(),
    email: Joi.string().required(),
    contact_no: Joi.string().required(),
    profile_pic_url: Joi.string().optional(),
    condo_room_id: Joi.string().optional(),
  }).options({allowUnknown:false}),
  options: {
    allowUnknown: true,
  },
};
