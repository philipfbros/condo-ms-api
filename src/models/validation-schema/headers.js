const Joi = require("joi");

module.exports = {
  headers: Joi.object({
    "x-api-key": Joi.string().required(),
    "user-token": Joi.string().required()
  }),
  options: {
    allowUnknown: true,
  },
};
