const Joi = require("joi");

module.exports = {
    headers: Joi.object({
      'x-api-key': Joi.string().required(),
      'user-token': Joi.string().required(),
    }),
  payload: Joi.object({
    user_id: Joi.string().required(),
    title: Joi.string().required(),
    message: Joi.string().required(),
  }).options({allowUnknown:false}),
  options: {
    allowUnknown: true,
  },
};
