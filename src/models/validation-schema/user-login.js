const Joi = require("joi");

module.exports = {
  headers: Joi.object({
    "x-api-key": Joi.string().required(),
  }),
  payload: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  }).options({ allowUnknown: false }),
  options: {
    allowUnknown: true,
  },
};
