
const { initDb } = require('../../services/database');
const Sequelize = require('sequelize');


const Accounts = require('./accounts')
const Announcements = require('./announcements')

const db = {}
const sequelize = initDb()



db.Accounts = Accounts(sequelize, Sequelize)
db.Announcements = Announcements(sequelize, Sequelize)




db.sequelize = sequelize
db.Sequelize = Sequelize



Object.keys(db).forEach(modelName => {
    if(db[modelName].associate){
        db[modelName].associate(db)
    }
})



module.exports = db;
