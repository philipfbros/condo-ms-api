const { Sequelize } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
    
    const Announcements = sequelize.define('announcements', {
            id : {
                type: Sequelize.UUID,
                primaryKey : true,
                defaultValue : Sequelize.UUIDV4,
            },
            user_id: DataTypes.STRING,
            title: DataTypes.STRING,
            message: DataTypes.STRING,
            },
        {   
        timestamps: true , paranoid: true
    })
  

    return Announcements
}