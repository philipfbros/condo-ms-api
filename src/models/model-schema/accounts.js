const { Sequelize } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
    
    const Accounts = sequelize.define('accounts', {
            id : {
                type: Sequelize.UUID,
                primaryKey : true,
                defaultValue : Sequelize.UUIDV4,
            },
            first_name: DataTypes.STRING,
            last_name: DataTypes.STRING,
            username: DataTypes.STRING,
            password: DataTypes.STRING,
            email: DataTypes.STRING,
            condo_room_id: DataTypes.STRING,
            contact_no: DataTypes.STRING,
            profile_pic_url: DataTypes.STRING,
            },
        {   
        timestamps: true , paranoid: true
    })
    // Users.associate = model => {
    //     Users.hasMany(model.UserAttendance, {
    //         as: 'attendance', foreignKey: 'approver_id'
    //     })
    //     Users.belongsTo(model.Position, {
    //         as: 'position', foreignKey: 'position_id'
    //     })
    //     Users.belongsTo(model.Department, {
    //         as: 'department', foreignKey: 'department_id'
    //     })
    // }

    return Accounts
}