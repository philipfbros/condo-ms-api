const { Accounts } = require("../models/model-schema");
const { Op } = require("sequelize");

const selectAccountByUsername = async (payload) => {
  return await Accounts.findOne({
    where: {
      [Op.or]: [{ username: payload.username }, { email: payload.email }],
    },
  });
};
const selectAccountById = async(id_)=>{
    return await Accounts.findOne({
        where: {
            id: id_
        }
    })
}
const createAccount = async (payload) => {
  return await Accounts.create(payload);
};
const getAllAccounts = async () => {
  return await Accounts.findAll({});
};
const deleteAccount = async (id_) => {
    await Accounts.destroy({where:{
        id: id_
    }});
  };
const loginAccount = async (payload) => {
  return await Accounts.findOne({
    where: {
      username: payload.username,
      password: payload.password,
    },
  });
};
module.exports = {
  selectAccountByUsername,
  createAccount,
  getAllAccounts,
  loginAccount,
  deleteAccount,
  selectAccountById,
};
