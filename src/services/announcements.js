const { Announcements } = require("../models/model-schema");
const { Op } = require("sequelize");



const selectAnnouncementById = async(id_)=>{
    return await Announcements.findOne({
        where: {
            id: id_
        }
    })
}

const createAnnouncement = async (payload) => {
  return await Announcements.create(payload);
};
const selectAllAnnoucement = async () => {
  return await Announcements.findAll({});
};
const deleteAnnouncement = async (id_) => {
    await Announcements.destroy({where:{
        id: id_
    }});
  };

module.exports = {
  selectAllAnnoucement,
  createAnnouncement,
  deleteAnnouncement,
  selectAnnouncementById,
};
