const JWT = require('jsonwebtoken')

module.exports = async (request, response) => {
    try {
        const autHeader = request.headers['user-token']
        const token = autHeader && autHeader.split(" ")[1]
        const result = JWT.verify(token, process.env.privateKEY, (err, res) => {
            if(err)
                return response.response('Invalid Token').code(403).takeover()
            return res.U
        })
        
        return result
    } catch(err) {
        return response.response('Something went wrong').code(500).takeover()
    } 
}