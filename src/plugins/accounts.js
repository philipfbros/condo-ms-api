const createAccountHandler = require('../handler/accounts/create-account').handler
const getAccountHandler = require("../handler/accounts/get-accounts").handler
const deleteAccountHandler = require("../handler/accounts/delete-account").handler
const loginHandler = require("../handler/accounts/user-login").handler

const validateApiKey = require('../middleware/validate-api-key')
const validateUserToken = require('../middleware/validate-user-token')

const createAccountSchema = require('../models/validation-schema/create-account')
const loginSchema = require('../models/validation-schema/user-login')
const headerSchema = require('../models/validation-schema/headers')
const deleteAccountSchema = require('../models/validation-schema/delete-account')

const accountsService = require('../services/accounts')
const token = require('../token/generate-token')

const md5 = require("md5")
module.exports = {
    name: 'accounts',
    register: async (server) => {
        server.decorate
        server.decorate('request','token',token)
        server.decorate('request','md5',md5)
        server.decorate('request','accountsService',accountsService)

        server.route({
            method : 'POST',
            path: '/v1/accounts',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },
                    {method: validateUserToken, assign: "u"}
                ],
                handler: createAccountHandler,
                validate: createAccountSchema,
            }
        })
        server.route({
            method : 'DELETE',
            path: '/v1/accounts/{accountId}',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },
                    {method: validateUserToken, assign: "u"}
                ],
                handler: deleteAccountHandler,
                validate: deleteAccountSchema,
            }
        })

        server.route({
            method : 'GET',
            path: '/v1/accounts',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },{
                        method: validateUserToken, assign:"u"
                    }
                ],
                handler: getAccountHandler,
                validate: headerSchema,
            }
        })

        server.route({
            method : 'POST',
            path: '/v1/auth/signIn',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },
                ],
                handler: loginHandler,
                validate: loginSchema,
            }
        })
    }
}