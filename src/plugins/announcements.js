const createAnnouncementHandler = require('../handler/announcements/create-announcement').handler
const getAnnouncementHandler = require("../handler/announcements/get-announcements").handler
const deleteAnnouncementHandler = require("../handler/announcements/delete-announcement").handler


const validateApiKey = require('../middleware/validate-api-key')
const validateUserToken = require('../middleware/validate-user-token')

const createAnnouncementSchema = require('../models/validation-schema/create-annoucement')
const headerSchema = require('../models/validation-schema/headers')
const deleteAnnouncementSchema = require('../models/validation-schema/delete-annoucement')

const announcementService = require('../services/announcements')

module.exports = {
    name: 'announcements',
    register: async (server) => {
        
        server.decorate('request','announcementService',announcementService)

        server.route({
            method : 'POST',
            path: '/v1/announcements',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },
                    {method: validateUserToken, assign: "u"}
                ],
                handler: createAnnouncementHandler,
                validate: createAnnouncementSchema,
            }
        })
        server.route({
            method : 'DELETE',
            path: '/v1/announcements/{announcementId}',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },
                    {method: validateUserToken, assign: "u"}
                ],
                handler: deleteAnnouncementHandler,
                validate: deleteAnnouncementSchema,
            }
        })

        server.route({
            method : 'GET',
            path: '/v1/announcements',
            options : {
                pre: 
                [
                    {
                        method: validateApiKey
                    },{
                        method: validateUserToken, assign:"u"
                    }
                ],
                handler: getAnnouncementHandler,
                validate: headerSchema,
            }
        })


    }
}