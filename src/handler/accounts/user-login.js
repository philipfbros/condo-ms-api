module.exports.handler = async (request, reply) => {
    try {
      const { payload, accountsService,token,md5 } = request;
      payload.password = md5(payload.password)
      const result = await accountsService.loginAccount(payload);

      if (!result)
        throw { message: "Login Failed", details: "Invalid username and password", code: 401 };
      const generateToken = token.generateToken(result.id)  
      return reply.response(generateToken).code(200);
    } catch (err) {
      let response = reply.response({
        message: `Internal Server Error`,
        details: "",
      });
      if (err.code) {
        response = reply
          .response({ message: err.message, details: err.details })
          .code(err.code);
      }
      return response;
    }
  };
  