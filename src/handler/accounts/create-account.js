module.exports.handler = async (request, reply) => {
    try {
      const { payload, accountsService,md5 } = request;
      const checkIfAccountExist = await accountsService.selectAccountByUsername(payload)
      if(checkIfAccountExist){
        throw { message: "Operation Failed", details: "Account already exist", code:401 }
      }
      payload.password = md5(payload.password)
      const result = await accountsService.createAccount(payload);
      return reply.response().code(201);
    } catch (err) {
      let response = reply.response({
        message: `Internal Server Error`,
        details: "",
      });
      if (err.code) {
        response = reply
          .response({ message: err.message, details: err.details })
          .code(err.code);
      }
      return response;
    }
  };
  