module.exports.handler = async (request, reply) => {
    try {
      const { payload, accountsService,md5,params } = request;
      const checkIfAccountExist = await accountsService.selectAccountById(params.accountId)
      if(!checkIfAccountExist){
        throw {message: "Operation Failed",details:"Account does not exist", code:401}
      }
      await accountsService.deleteAccount(params.accountId)
      return reply.response().code(201);
    } catch (err) {
      let response = reply.response({
        message: `Internal Server Error`,
        details: "",
      });
      if (err.code) {
        response = reply
          .response({ message: err.message, details: err.details })
          .code(err.code);
      }
      return response;
    }
  };
  