module.exports.handler = async (request, reply) => {
    try {
      const { payload, announcementService,md5 } = request;
      await announcementService.createAnnouncement(payload);
      return reply.response().code(201);
    } catch (err) {
      let response = reply.response({
        message: `Internal Server Error`,
        details: "",
      });
      if (err.code) {
        response = reply
          .response({ message: err.message, details: err.details })
          .code(err.code);
      }
      return response;
    }
  };
  