module.exports.handler = async (request, reply) => {
    try {
      const { announcementService } = request;
      
      const result = await announcementService.selectAllAnnoucement();
      if (!result)
        throw { message: "Something went wrong", details: "", code: 401 };
  
      return reply.response(result).code(200);
    } catch (err) {
      let response = reply.response({
        message: `Internal Server Error`,
        details: "",
      });
      if (err.code) {
        response = reply
          .response({ message: err.message, details: err.details })
          .code(err.code);
      }
      return response;
    }
  };
  